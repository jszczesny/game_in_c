
#ifndef SNAKE_H
#define SNAKE_H

#include <stdint.h>

typedef unsigned int uint;

typedef struct vector2d{
    uint8_t x;
    uint8_t y;
}Vector;

void clrscr();
void print_array(char *arr);
void clear_arr(char *arr);

void add_snake_body(char *arr, uint8_t *body_x, uint8_t *body_y, uint length);
void add_snake_head(char *arr, uint8_t *body_x, uint8_t *body_y, uint length);
void clear_snake(char *arr, uint8_t pos_x, uint8_t pos_y);
void add_food(char *arr, uint8_t pos_x, uint8_t pos_y);
char isCollision(char *arr, uint8_t pos_x, uint8_t pos_y);

void snake(void);


#endif
