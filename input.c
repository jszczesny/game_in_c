//Linux Libraries
#include "input.h"

//Linux Functions - These functions emulate some functions from the windows only conio header file
int kbhit(void)
{
    struct termios oldt, newt;
    int ch;
    int oldf;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    ch = getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    fcntl(STDIN_FILENO, F_SETFL, oldf);

    if(ch != EOF)
    {
        ungetc(ch, stdin);
        return 1;
    }

    return 0;
}

char getch()
{
    char c;
    system("stty raw");
    c = getchar();
    system("stty sane");
    return(c);
}

char checkKeysPressed()
{
    if(kbhit()) //If a key has been pressed
    {   
        uint8_t tmp = getch();     
        if(tmp == EXIT_BUTTON){
            getch(); 
            return getch();
        }
        return tmp;            
    }  
    else
        return 0; 
}

//End linux Functions
