
#ifndef SPACE_INVADERS_H
#define SPACE_INVADERS_H

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>

typedef struct vector2D
{
    uint8_t x;
    uint8_t y;
}Vector;

void clrscr();
void print_array(char *arr);
void add_invaders(char *arr, char *invaders, uint8_t length);
void add_projectile(char *arr, Vector *projectile, uint8_t length);
void add_player(char *arr, uint8_t size_x, uint8_t size_y, int x, int y);
char isCollision(char *arr, uint8_t pos_x, uint8_t pos_y);
void clear_arr(char *arr);
void space_invaders_start(void);

#endif