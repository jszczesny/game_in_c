#ifndef INPUT_H
#define INPUT_H

#define EXIT_BUTTON     27 //ESC
#define SPACE_BUTTON    30 
#define UP_ARROW        'A'
#define LEFT_ARROW      'D'
#define RIGHT_ARROW     'C'
#define DOWN_ARROW      'B'
#define ENTER_KEY       10

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

int kbhit(void);
char getch();
char checkKeysPressed();

#endif
