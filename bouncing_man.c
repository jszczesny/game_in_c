#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>

#define SIZE_X 50
#define SIZE_Y 20



typedef struct vector2D
{
    int x;
    int y;
}Vector;

void print_array(char *arr, uint8_t size_x, uint8_t size_y)
{
    for (int i = 0; i < size_y; i++)
    {
        for (int j = 0; j < size_x; j++)
        {
           printf("%c", arr[j]);
        }
        printf("\n"); 
        arr += size_x;       
    }
}

void add_player(char *arr, uint8_t size_x, uint8_t size_y, int x, int y)
{
    char player[4][5] =
    {
            {'(','^','_','^',')'},
            {'\\','s','i','i','/'},
            {' ',' ','|',' ',' '},
            {' ','/',' ','\\',' '}
    };
    for (int i = 0; i < 4; i++)
    {   
        
        for (int j = 0; j < 5; j++)
        {
            *((arr + x+j)+(size_x*(i+y))) = player[i][j];
        }       
         
    }
    
}

void clear_array(char *arr, uint8_t size_x, uint8_t size_y)
{
    for (int i = 0; i < size_y; i++)
    {
        for (int j = 0; j < size_x; j++)
        {
            if(i==0 || j==0 || i==size_y-1 || j==size_x-1)
                arr[j] = '*';
            else
                arr[j] = ' ';
        }
        arr += size_x;        
    } 
}

int man_main(void)
{
    char world_arr[SIZE_Y][SIZE_X] = {0};
    Vector dir;
    dir.x = 1;
    dir.y = 1;
    int pos_x = 5, pos_y = 6;
    
    while(1)
    {
        clear_array((char*)world_arr, SIZE_X, SIZE_Y);
        pos_x += dir.x;
        pos_y += dir.y;
        add_player((char*)world_arr, SIZE_X, SIZE_Y, pos_x, pos_y);
        if(pos_x+7 > SIZE_X)
        {
            dir.x = -1;
        }
        if(pos_x-2 < 0)
        {
            dir.x = 1;
        }
        if(pos_y+6 > SIZE_Y)
        {
            dir.y = -1;
        }
        if(pos_y-2 < 0)
        {
            dir.y = 1;
        }
        print_array((char*)world_arr, SIZE_X, SIZE_Y);
        usleep(100000);
        system("clear");
    }
    return 0;
}