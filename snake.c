#include <stdio.h>
#include <time.h>

#include "input.h"
#include "snake.h"

// World size
#define SIZE_X 30
#define SIZE_Y 15

//Linux Constants
//Controls (arrow keys for Manjaro)

#define SNAKE       '@'
#define WALL        '|'
#define CEIL        '-'
#define FOOD        '+'
#define BLANK       ' '

#define RED         "\x1b[31m"
#define BLUE        "\x1b[34m"
#define RESET       "\x1b[0m"



void clrscr()
{
    system("clear");
    return;
}

//This function checks if a key has pressed, then checks if its any of the arrow keys/ p/esc key. It changes direction acording to the key pressed.


void print_array(char *arr)
{
    for (int i = 0; i < SIZE_Y; i++)
    {
        for (int j = 0; j < SIZE_X; j++)
        {
            if (arr[j] == SNAKE || arr[j] == FOOD)
                fprintf(stdout, "\x1b[42m " RESET);
            else  if (arr[j] == WALL) 
                fprintf(stdout, "\x1b[44m " RESET);
            else 
                fprintf(stdout, " ");


        }
        fprintf(stdout, "\n"); 
        arr += SIZE_X;       
    }
}

void clear_arr(char *arr)
{
    for (uint8_t i = 0; i < SIZE_Y; i++)
    {
        for (uint8_t j = 0; j < SIZE_X; j++)
        {
            if(i==0 || j==0 || i==SIZE_Y-1 || j==SIZE_X-1)
                arr[j] = WALL;
            else
                arr[j] = BLANK;
        }
        arr += SIZE_X;
    }
    
}

void add_snake_body(char *arr, uint8_t *body_x, uint8_t *body_y, uint length)
{      
    for (uint8_t i = 0; i < length; i++)
    {   
        *((arr + body_x[i])+(SIZE_X*(body_y[i]))) = SNAKE;
    }
    
}
void add_snake_head(char *arr, uint8_t *body_x, uint8_t *body_y, uint length)
{
    *((arr + body_x[length])+(SIZE_X*(body_y[length]))) = SNAKE;
}


void clear_snake(char *arr, uint8_t pos_x, uint8_t pos_y)
{
    *((arr + pos_x)+(SIZE_X*(pos_y))) = ' ';
}

void add_food(char *arr, uint8_t pos_x, uint8_t pos_y)
{
    *((arr + pos_x)+(SIZE_X*(pos_y))) = FOOD;
}

char isCollision(char *arr, uint8_t pos_x, uint8_t pos_y)
{
    
    char tmp = *(arr+pos_x+(SIZE_X*pos_y));

    if (tmp != BLANK)
    {
        return tmp;
    };  
}

void snake(void)
{
    char flag = DOWN_ARROW;
    char prev_flag = 0;
    Vector direction;
    direction.y = 1;
    int gameover = 1;
    uint length = 0;
    uint8_t pos_x = SIZE_X / 2, pos_y = SIZE_Y / 2; 
    uint8_t food_pos_x = 0, food_pos_y = 0; 
    uint8_t body_pos_x[SIZE_X*SIZE_Y], body_pos_y[SIZE_X*SIZE_Y]; 
    uint8_t isFood = 0;

    srand((uint)time(NULL));

    char world_arr[SIZE_Y][SIZE_X] = {0};

    do{
        usleep(180000);

        clrscr();            
        
        flag = checkKeysPressed();

        switch (flag)
        {
            case DOWN_ARROW:
                if (prev_flag != UP_ARROW){
                    direction.x = 0;
                    direction.y = 1;
                    prev_flag = flag;
                };
                break;
            case UP_ARROW:
                if (prev_flag != DOWN_ARROW){
                    direction.x = 0;
                    direction.y = -1;
                    prev_flag = flag;
                };
                break;
            case RIGHT_ARROW:
                if (prev_flag != LEFT_ARROW){
                    direction.x = 1;
                    direction.y = 0;
                    prev_flag = flag;
                };
                break;
            case LEFT_ARROW:
                if (prev_flag != RIGHT_ARROW){
                    direction.x = -1;
                    direction.y = 0;
                    prev_flag = flag;
                };
                break;   
            case 'x':
                gameover = 0;
                break;          
            default:
                break;
        }

        pos_x += direction.x;
        pos_y += direction.y;


        for (uint8_t i = 0; i < length; i++)
        {
            body_pos_x[i] = body_pos_x[i+1];
            body_pos_y[i] = body_pos_y[i+1]; 
        }
        
        body_pos_x[length] = pos_x;
        body_pos_y[length] = pos_y;
       

        if(!isFood){            
            label3:
                food_pos_x = rand() % (SIZE_X-1);
                if(food_pos_x == 0)
                    goto label3;
            label4:
                food_pos_y = rand() % (SIZE_Y-1);
                if(food_pos_y == 0)
                    goto label4;
            isFood = 1;
        }

        add_food((char*)world_arr, food_pos_x, food_pos_y);
        if (length > 0)
            add_snake_body((char*)world_arr, body_pos_x, body_pos_y, length);

        switch(isCollision((char*)world_arr, pos_x, pos_y))
        {
            case FOOD:            
                isFood = 0;
                length++;
                break;
            case WALL:
                gameover = -2;
                break;
            case SNAKE:
                gameover = -3;
                break;
            default:
                break;
        }
        if (!isFood)
            add_snake_head((char*)world_arr, body_pos_x, body_pos_y, length-1);
        else
            add_snake_head((char*)world_arr, body_pos_x, body_pos_y, length);


        print_array((char*)world_arr);

        clear_arr((char*)world_arr);

    }while(gameover > 0);

    printf("\n^^^^^^^^^^ GAME OVER ^^^^^^^^^^^^\n\n");
    printf("Reasson is %d\n", gameover);
}