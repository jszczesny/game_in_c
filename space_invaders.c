#include "space_invaders.h"
#include "input.h"

#define SIZE_X 50
#define SIZE_Y 50

#define SNAKE       '@'
#define WALL        '|'
#define CEIL        '-'
#define FOOD        '+'
#define BLANK       ' '

void clrscr()
{
    system("clear");
    return;
}

void clear_arr(char *arr)
{
    for (uint8_t i = 0; i < SIZE_Y; i++)
    {
        for (uint8_t j = 0; j < SIZE_X; j++)
        {
            if(i==0 || j==0 || i==SIZE_Y-1 || j==SIZE_X-1)
                arr[j] = WALL;
            else
                arr[j] = BLANK;
        }
        arr += SIZE_X;
    }    
}


void print_array(char *arr)
{
    fprintf(stdout, "\n"); 

    for (int i = 0; i < SIZE_Y; i++)
    {
        for (int j = 0; j < SIZE_X; j++)
        {
            fprintf(stdout, "%c", arr[j]);
        }
        fprintf(stdout, "\n"); 
        arr += SIZE_X;       
    }
}
void add_projectile(char *arr, Vector *projectile, uint8_t length)
{
    for (int i = 0; i < length; i++)
    {
        *((arr + projectile[i].x)+(SIZE_X*projectile[i].y)) = '*';         
    }
}
void add_invaders(char *arr, char *invaders, uint8_t length)
{
    for (int i = 1; i < SIZE_Y-1; i++)
    {
        for (int j = 1; j < SIZE_X-1; j++)
        {
            *((arr + j)+(SIZE_X*i)) = *((invaders + j)+(SIZE_X*i));
        }
    }
}

void add_player(char *arr, uint8_t size_x, uint8_t size_y, int x, int y)
{
    char player[2][3] =
    {
            {' ','^',' '},
            {'/','^','\\'}
    };
    for (int i = 0; i < 2; i++)
    {   
        
        for (int j = 0; j < 3; j++)
        {
            *((arr + x+j)+(size_x*(i+y))) = player[i][j];
        }       
         
    }
}

char isCollision(char *arr, uint8_t pos_x, uint8_t pos_y)
{    
    char tmp = *(arr+pos_x+(SIZE_X*pos_y));

    if (tmp != BLANK)
    {
        return tmp;
    };  
}

    

void space_invaders_start(void)
{
    int8_t gameover = 1;
    uint8_t frame_cnt = 0;
    uint8_t direction = 1;
    char flag = RIGHT_ARROW;
    char world_arr[SIZE_Y][SIZE_X] = {0};
    Vector projectile[SIZE_Y];
    char invaders[SIZE_Y][SIZE_X];
    uint8_t projectile_size = 0;
    uint8_t invaders_size = 50;
    uint8_t pos_x = 5, pos_y = SIZE_Y - 4;

    for (uint8_t i = 0; i < SIZE_Y; i++)
    {
        for (uint8_t j = 0; j < SIZE_X; j++)
        {
                invaders[i][j] = BLANK;           
        }
    }
       
    for (uint8_t i = 4; i < 22; i+=4)
    {
        for (uint8_t j = 4; j < SIZE_X - 8; j+=4)
        {
            invaders[i][j] = '@';            
            invaders[i][j + 1] = '@';            
            invaders[i+1][j] = '@';           
            invaders[i+1][j+1] = '@';           
        }
        
    }
    
    
    do{
        usleep(50000);
        clear_arr((char*) world_arr);
        clrscr();            
        
        // flag = checkKeysPressed();

        switch (checkKeysPressed())
        {
            case RIGHT_ARROW:
                if (pos_x < SIZE_X - 4)
                    pos_x+=2;                
                break;
            case LEFT_ARROW:
                if (pos_x > 1)
                    pos_x-=2;                   
                break;  
            case 'a':
                projectile[projectile_size].x = pos_x+1;
                projectile[projectile_size].y = pos_y;
                projectile_size++;
                break; 
            case 'x':
                gameover = 0;
                break;          
            default:
                break;
        }
        if (frame_cnt > 10){
            if(direction){
                for (uint8_t i = SIZE_Y-1; i > 0; i--)
                {
                    for (uint8_t j = SIZE_X-1; j > 0; j--)
                    {
                        invaders[i][j] = invaders[i][j-1];           
                    }
                    if(invaders[i][SIZE_X - 3] == '@')                    
                        direction = 0;
                }
                if(!direction){
                    for (uint8_t i = SIZE_Y; i > 0; i--)
                    {
                        for (uint8_t j = SIZE_X; j > 0; j--)
                        {
                            invaders[i][j] = invaders[i-1][j];           
                        }
                    }
                }

            }else{
                for (uint8_t i = 0; i < SIZE_Y-1; i++)
                {
                    for (uint8_t j = 0; j < SIZE_X-1; j++)
                    {
                        invaders[i][j] = invaders[i][j+1];           
                    }
                    if(invaders[i][3] == '@')
                        direction = 1;
                     
                }
                if(direction){
                    for (uint8_t i = SIZE_Y; i > 0; i--)
                    {
                        for (uint8_t j = SIZE_X; j > 0; j--)
                        {
                            invaders[i][j] = invaders[i-1][j];           
                        }
                    }
                }
            }
            frame_cnt = 0;
        }

        add_invaders((char*) world_arr, (char*)invaders, invaders_size);
        for (uint8_t i = 0; i < projectile_size; i++)
        {
            projectile[i].y--;
        }
        for (uint8_t i = 0; i < projectile_size; i++)
        {
            if (isCollision((char*) world_arr, projectile[i].x, projectile[i].y) == WALL)
            {
                for (uint8_t j = 0; j < projectile_size; j++)
                {
                    projectile[j] = projectile[j+1];
                }
                projectile_size--;
            }
            if (isCollision((char*) world_arr, projectile[i].x, projectile[i].y) == '@')
            {
                for (uint8_t x = projectile[i].x-2; x < projectile[i].x + 2; x++)
                {
                    for (uint8_t j = projectile[i].y-2; j < projectile[i].y + 2; j++)
                    {
                        invaders[j][x] = BLANK;
                    }
                                     
                }
                for (uint8_t j = 0; j < projectile_size; j++)
                {
                    projectile[j] = projectile[j+1];
                }
                projectile_size--;
            }
        }
        add_projectile((char*) world_arr, (Vector*)projectile, projectile_size);        
        add_player((char*) world_arr, SIZE_X, SIZE_Y, pos_x, pos_y);        
        print_array((char*) world_arr);
        frame_cnt++;
    } while (gameover > 0);


}